import { useState, useRef } from "react"
import ImagesFullScreen from "../components/reusable/imagesFullScreen"
import { deleteArrayValue } from "./functions"

export const useImages = (initialImgArr=[], onClickF = f => f) => {
    const [images, setImages] = useState(initialImgArr)

    return[
        {   
            onClick: e => {
                return  onClickF (<ImagesFullScreen images={images} actIndex={e.target.alt} close={onClickF} />)
            },
            onError: e =>  {
                const newArr = deleteArrayValue(images, e.target.src)
                setImages(newArr)
            }
        },
        images
    ]
}

export const useRefCustom = (refValue) => {
    const ref = useRef(refValue)

    return[
        {
            onClick: () => ref.current.scrollIntoView()
        },
        ref
    ]
} 

export const useInput = (initialValue, { type='text', required=true}) => {
    const[value, setValue] = useState(initialValue)

    return[
        {
            value,
            type,
            required,
            onChange: (e) => setValue(e.target.value)
        },
        () => setValue(initialValue)
    ]
}