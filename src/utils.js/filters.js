import { sortArrayByPrice } from "./functions"

// Genera un array con el nombre cada tipo diferente del campo seleccionado {cutBy}
function cutUpArray (array=[], cutBy){
    const namesArr = []
    array.forEach( o => {
        if(!namesArr.includes(o[cutBy])) namesArr.push(o[cutBy])
    })
    return namesArr
}


/**
 * genera un array por cada campo en arrayNames
 * arrayNames.length *  originalArray.length
 * @param {Array of Strings} arrayNames 
 * @returns {Matriz}
 */
function cutUpArrayBy (arrayNames=[], cutBy, originalArray=[]){
    const arrays = arrayNames.map(a => new Array(0))

    for(let i = 0; i < arrayNames.length;  i++){
        for(let j = 0; j < originalArray.length; j++){
            if(originalArray[j][cutBy] === arrayNames[i]) {
                arrays[i].push(originalArray[j]) 
            }
        } 
    }
    return arrays
}


function sortAllArraysBy (arrayOfArrays, sortBy='Price'){
    let sorted

    switch (sortBy) {
        case 'Price':
            sorted = arrayOfArrays.map( (array) => sortArrayByPrice(array))
            break;
        default:
            break;
    }
    return sorted
}

/**
 * @param {Array} hotelsArr, array de ListaPrecios a ordenar  
 * @returns {Array of arrays} devuelve un array por cada BoardName diferente y todos ordenados por precio
 */

export const sortHotelBoardNamesByPrice = (hotelsArr, cutBy='BoardName', sortBy='Price' ) => sortAllArraysBy(cutUpArrayBy(cutUpArray(hotelsArr, cutBy), cutBy, hotelsArr ))

/**
 * 
 * @param {*} hotelsArr array que quieres partir y ordenar
 * @param {*} cutBy (Field) Parametro por el que quieres partir el array
 * @returns  {Array of arrays} devuelve un array de arrays, 1 por cada tipo de Field diferente y todos ordenados por precio.
 */
export const sortHotelFieldByPrice = (hotelsArr, cutBy='BoardName', sortBy='Price' ) => sortAllArraysBy(cutUpArrayBy(cutUpArray(hotelsArr, cutBy), cutBy, hotelsArr ))

export const spliceArrayByBoardName = (hotelsArr) => cutUpArrayBy(cutUpArray(hotelsArr, 'BoardName'), 'BoardName', hotelsArr)