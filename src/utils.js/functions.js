export function setStringMaxLenght(string, maxLength){
    return string.length > maxLength ? string.slice(0, maxLength) + '...' : string;
}

export function sortArrayByPrice(arrayToReduce=[], cuantity){
    
    const newArray = arrayToReduce.sort( (a, b) => {
        if (parseFloat(a.Price) > parseFloat(b.Price)) {
        return 1
        }
        if (parseFloat(a.Price) < parseFloat(b.Price)) {
        return -1
        }
        return 0
    })

    return newArray.slice(0, cuantity ? cuantity: undefined)

}


export function ArrayFromNumber (...values){

    const arrays = values.map((value) => 
        typeof value === 'string' ? 
        Array(parseInt(value)).fill()
        : typeof value === 'number' ?
        Array(value).fill() 
        : null
    )

    return arrays
}


export function deleteArrayValue(array, valueToPop){
    const newArray = array.filter( e => e!== valueToPop)
    return newArray
}

export function deleteArrayItemByObjectKeyValue (array=[], key, value ){
    return array.filter(o => o[key] !== value )
}

export const arrayFindValueByField = (array=[], field=String, value) => (
    array.find( item => item[field] === value)
)


export const priceSum = array => array.reduce( (total, current) =>
    typeof current['Price'] === 'string' ? 
        total + parseInt(current['Price']):
    typeof current['Price'] === 'number' ?
        total + current['Price']
    : null
, 0 )

export const guestSum = array => array.reduce( (total, current) =>  
    total + current['adults'] + current['kids'].length
, 0)



export function searchDataBuilder ({data:{dateFrom=Date, dateTo=Date,guests=Array, city=String, hotel=String } }) {

    return {
        dates:{
            from: dateFrom,
            to: dateTo
        },
        guests: guests,
        city: city,
        hotel: hotel,

    }
}
