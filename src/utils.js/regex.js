export const validateEmail = new RegExp(
    /^[a-zA-Z0-9]+\@{1}[a-zA-Z]{1,20}\.+[a-zA-Z]{2,3}$/
)

export const validateTlf = new RegExp(
    /^\+{1}[0-9]{2,3}[0-9]{9}$/
)
