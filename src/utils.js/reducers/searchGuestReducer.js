import React, {useReducer} from "react"

const initialState = [
    {
        adults: 2,
        kids:[
            {age:10}
        ]
    },

]
   
        

const room = {
    adults: 2,
    kids: [
        {age:10}
    ]
}

const kid = {
    age: 10,
}


const searchGuestReducer = (state, action) => {
    const roomI = action.payload.room 

    switch (action.type) {
        case 'addKid':
        return state.map((room, i) => i === roomI ? {...state[i], kids:[...state[i].kids, kid]} : room )
        
        case 'deleteKid':

            return  (
                state.map((room, i) => i===roomI & room.kids.length > 0 ? {
                    ...state[i], kids: room.kids.filter((kid, i) => i != action.payload.kid)
                } : room)
            )   
            
        case 'addKidAge':

            return(
                state.map((room, i) => i === roomI ? {
                    ...state[i], kids: room.kids.map((kid, j) => j === action.payload.kid ? {age: action.payload.age} : kid) 
                } : room)
            )
        case 'addAdult':
          return state.map((room, i) => i === roomI ? {...state[i], adults: state[i].adults +1}: room)

        case 'deleteAdult':
            
            return (
                state[roomI].adults > 0 ?
                state.map((room, i) => i === roomI ? {...state[i], adults: state[i].adults -1}: room):
                state
            )
              
        case 'addRoom':
                  
            return [...state, room]
        case 'deleteRoom':
          return state.filter((room, index) => index !== action.payload.room )   
                
      default:
        break;
    }
}


export const useGuestDispatcher = dispatcher => {

    return{
        addkid: function (roomIndex){
            dispatcher({type:'addKid', payload:{room: roomIndex}})
        },

        deleteKid: function (roomIndex, kidIndex) {
            dispatcher({type:'deleteKid', payload:{room:roomIndex, kid: kidIndex}})
        },

        addKidAge: function(roomIndex, kidIndex, age) {
            dispatcher({type:'addKidAge', payload:{room:roomIndex, kid: kidIndex, age: age}})
        },

        addAdult: function(roomIndex){
            dispatcher({type:'addAdult', payload:{room:roomIndex}})
        },
        deleteAdult: function(roomIndex){
            dispatcher({type:'deleteAdult', payload:{room:roomIndex}})
        },

        addRoom: function(){
            dispatcher({type:'addRoom', payload:{room: 0}})
        },
        deleteRoom: function(roomIndex){
            dispatcher({type:'deleteRoom', payload:{room:roomIndex}})
        }
    }
}


export default function useGuestReducer(){
    const [state, dispatch] = useReducer(searchGuestReducer, initialState)
    return {
        roomState:state,
        dispatchFunctions:useGuestDispatcher(dispatch)
    }
}


