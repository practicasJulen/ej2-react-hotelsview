import { deleteArrayItemByObjectKeyValue } from "./functions"
import { useReducer } from "react"


export const reducerIndex = (state, action) => {
  const {max, min, actual} = state

  switch(action.type){
    case 'next':
      if(actual < max) return {...state, actual: actual + 1}
      else return state
    case 'preview':
      if(actual > min) return {...state, actual: actual -1}
      else return state
    default:
      break;
  }
}


export const basketReducer = (state, action) =>{
  switch (action.type) {
    case 'add':
      return [...state, {...action.payload, idBasket: state.length + 1 + Math.round(Math.random() * 10000)}]
    
    case  'delete':
      const newState = deleteArrayItemByObjectKeyValue(state, action.payload.key, action.payload.value)
      return [...newState]
    
    default:
      return state
  }
}


const useBasketDispatcher =  dispatcher => ({

  addToBasket:function (item) {
    dispatcher({type:'add', payload:{item}})
  },
  deleteFromBasket: function (val) {
    dispatcher({type:'delete', payload:{value: val}})
  }

})


export const useBasketReducer = () => {
  const [basket, dispatch]  = useReducer(basketReducer, [])
  return {
    basketState: basket,
    dispatchFunctions: useBasketDispatcher(dispatch)

  }
}

export const useBasket = useBasketReducer
