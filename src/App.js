
import './App.css';
import HotelView from './components/hotelsView';

function App() {
  return (
    <div className="App">
      <HotelView />
    </div>
  );
}

export default App;
