export  interface Hotel{
    CategoryName: string,
    City: string,
    StateName: string,
    CommonId: number,
    idHotel: string,
    Dir: string,
    Email: string,
    Fax: string,
    Lat: string,
    Long: string,
    Tfno: string,
    NombreHotel: string,
    Zipcode: string,
    ListFotos: Array<string>,
    ListaPrecios: Array<Producto>
    ShortDesc: string
}


export interface Producto {
    BoardName: string,
    Cancelattion: string,
    CancelationCode: string,
    CategoryCode: string,
    Currency: string,
    Price: string,
    NoReembolsable: boolean,
    Name: string,
    NumAdults: string,
    NumChilds: string,
    id: number

    addToBasket?:  (...params: any) => void
}

const prueba = (params: Array<any>): void =>   {
    params.forEach((param) => console.log(param)) 
}

export interface Contacto{
    Tfno: string,
    Fax: string,
    Email: string
}

export interface Informacion{
    ShortDesc: string,
    Dir: string,
    City: string,
    StateName: string,
    ZipCode: string

    goTo?: (...params: any) => void
}

export interface Ubicacion{
    Lat: string,
    Long: string
}

