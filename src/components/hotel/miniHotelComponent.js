import React, {} from "react";
import { sortHotelBoardNamesByPrice } from "../../utils.js/filters";
import '../Css/miniHotel/miniHotel.css'
import HotelBasicInfo from "./hotelComponents/hotelBasicInfo";
import HotelImages from "./hotelComponents/hotelImages";
import HotelPrices from "./hotelComponents/hotelPrices";
import { MiniHotelTs } from "./hotelComponents/mobileComponents/mobMiniHotel/miniHotel";


export default function MiniHotel({data={}, setView = f => f, testState}){
    console.log(data)
    const {ListFotos, idHotel} = data
    const sortedPriceList = sortHotelBoardNamesByPrice(data.ListaPrecios)
    return(
        <>
            {
                testState ?  (
                <div className="miniHotelContainer">
                    <div id="first">
                        <HotelImages images={ListFotos}/>
                    </div>
                    <div id="second">
                        <HotelBasicInfo data={data} renderDescr={false}/>
                    </div>
                    <div id="third">
                        <div id="precio">
                            <div><HotelPrices prices={sortedPriceList}/></div>
                        </div>
                        <div id="botonInfo">
                            <button value={idHotel} onClick={ (e) => setView({idHotel: e.target.value})}>Más info</button>
                        </div>
                    </div>
                </div>) : <MiniHotelTs hotel={data} setView={setView} />
            }

        </>
    )
}