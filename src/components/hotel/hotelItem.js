import React, {} from 'react'
import HotelAdditionalInfo from './hotelComponents/hotelAdditionalInfo'
import HotelBasicInfo from './hotelComponents/hotelBasicInfo'
import HotelImages from './hotelComponents/hotelImages'
import HotelProductList from './hotelComponents/hotelProductList'
import '../Css/hotelItem.css'

export default function Hotel({data={}, setView = f => f}){
    const {ListFotos, ListaPrecios, idHotel} = data

    return(
        <>
            <div className="HotelContainer" >
                <div className="HotelTop">
                    <HotelImages images={ListFotos}/>
                    <HotelBasicInfo data={data}/>
                </div>
                <div className="HotelBottom">
                    <HotelAdditionalInfo  data={data}/>
                    <HotelProductList productList={ListaPrecios}/>
                    <div id="btndiv">
                        <button id="specialbutton" value={idHotel} onClick={() => setView({idHotel})}>{ListaPrecios.length -2}  options</button>
                    </div>
                </div>
            </div>
        </>
    )
}