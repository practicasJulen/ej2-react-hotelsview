import React from 'react'
import BasketForm from '../reusable/forms/basketForm'
import BasketItems from './basket/basketItems'

export default function BasketPage({basketData=[], deleteItem=f=>f}) {
    console.log(basketData)
    return (
        <>  
            <div>
                <BasketItems items={basketData} deleteItem={deleteItem}/>
            </div>
            <div style={{display:'flex', flex:1, justifyContent:'center', alignItems:"center", padding:"2%"}}>
                <BasketForm />        
            </div>
        </>
    )
}
