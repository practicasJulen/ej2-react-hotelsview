import React, { useRef, useEffect } from "react";
import HotelBasicInfo from "./hotelComponents/hotelBasicInfo";
import ImagesComponent from "./singleHotel/imagesComponent";
import '../Css/singleHotel/singleHotel.css'
import UbicationComponent from "../reusable/ubicationComponent";
import ProductListComponent from "./singleHotel/productListComponent";
import ContactComponent from "../reusable/contactComponent";
import { useRefCustom } from "../../utils.js/customHooks";
import ColumnInformationComponent from "../reusable/columnInformationComponent";


export default function SingleHotel({data={}, addToBasket=f=>f}){

    const {ListFotos, ListaPrecios, ShortDesc, Lat, Long, Tfno, Email, Fax, Dir, idHotel } = data
    const ubication = {Lat, Long, Dir}
    const maincontainerRef = useRef()
    const [ubiBtnProps, ubicationRef] = useRefCustom()
    const [alojamientosBtnProps, alojamientosRef] = useRefCustom()

    useEffect(() => {
        maincontainerRef.current.scrollIntoView()
    }, [])

    return(
        <>
        <div ref={maincontainerRef} className="SingleHotelContainer">
            <div id="fotoAndInfo">
                <ImagesComponent images={ListFotos}/>
                <div id="InfoAndContact">
                    <div id="Info">
                        <HotelBasicInfo data={data} renderDescr={false}/>
                    </div>
                    <div id="Contact" >
                        <ColumnInformationComponent alojBtnPropsRef={alojamientosBtnProps} ubiBtnPropsRef={ubiBtnProps}/>
                        <ContactComponent options={{Tfno, Email, Fax}} />
                    </div>
                </div>
            </div>
            <div id="descriptionAndUbication" >
                <div id="description">
                    <p>"{ShortDesc}"</p>
                </div>
                <div id="ubication" ref={ubicationRef}>
                    <UbicationComponent {...ubication}/>
                </div>
            </div>
            <div id="productList" ref={alojamientosRef}>
                <ProductListComponent productList={ListaPrecios} idHotel={idHotel} addToBasket={addToBasket}/>
            </div>
        </div>
        </>
    )
}