import React from 'react'
import ReactHtmlParser from 'react-html-parser'

export default function HotelProductOptions({NoReembolsable, Cancelation}) {
    return (
        <div className="productFiel">
            <div className="ProductHeader">
                <p>Cancelación</p>
            </div>
            <div className="ProducInfo">
                <p style={{textDecorationLine:"underline"}}>{NoReembolsable ? "No reembolsable" : "Reembolsable"}</p>
            </div>
            <div>
                {ReactHtmlParser(Cancelation)}
            </div>
        </div>
    )
}
