import React, { useState } from "react";
import { useImages } from "../../../utils.js/customHooks";
import ImageComponent from "../../reusable/imageComponent";
import '../../Css/singleHotel/singleHotelImages.css'
import ImagesFullScreen from "../../reusable/imagesFullScreen";
import { BsCamera, BsCameraFill } from "react-icons/bs";

export default function ImagesComponent({images=[], qty=6, mobile=false}){
    const [imagesWindow, setWindow] = useState()
    const [imageProps, arrayImages] =  useImages(images, setWindow)
    const [mainImg, ...littleImgs] = arrayImages
    const arrayToMap = littleImgs.length > qty ? littleImgs.slice(0, qty ) : littleImgs

    console.log(qty)

    return(
        <>
        {
            imagesWindow ? imagesWindow : null 
        }
        <div className={mobile? "MobileImageContainer": "ImagesContainer"}>
            <div id="big">
                <ImageComponent url={mainImg} props={imageProps} width={"95%"} height={"95%"} value={-1}/>
            </div>
            <div id="littles">
                {
                    arrayToMap.map( (e, i) => i !== qty-1 ?  (<ImageComponent key={i} value={i} props={imageProps} url={e} width={"40%"} height={"25%"} />) :
                        (<div onClick={() => setWindow(<ImagesFullScreen images={arrayImages} actIndex={5} close={() => setWindow()} />)}style={{display:'flex', flex: 1}}><ImageComponent key={i} value={i} props={imageProps} url={e} width={"40%"} height={"25%"} />  {littleImgs.length > qty ? <button  style={{position:'absolute', display:'flex', marginLeft: '7%'}} >{arrayImages.length - qty -1} More <BsCameraFill /></button> : null}</div>)
                    )
                }
            </div>
        </div>
        </>
    )
}