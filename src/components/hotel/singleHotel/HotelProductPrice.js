import React from 'react'

export default function HotelProductPrice({Currency, Price, product, addToBasket=f=>f}) {
    const buy = () => addToBasket(product)
    
    return (
        <div className="productFiel">
            <div className="ProductHeader">
                <p>precio</p>
            </div>
            <div className="ProducInfo">
                <p id="cantidad">{parseInt(Price)}</p>
                <p id="moneda">{Currency}</p>
            </div>
            <div id="Btn-reservar">
                <button onClick={ () => buy()}>Hacer reserva</button>
            </div>
        </div>
    )
}
