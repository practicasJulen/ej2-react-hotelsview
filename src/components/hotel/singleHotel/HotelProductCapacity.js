import React from 'react'
import Personicons from '../../reusable/personIconsComponent'

export default function HotelProductCapacity({NumAdults, NumChilds}) {
    return (
        <div className="productFiel">
            <div className="ProductHeader">
                <p>Capacidad</p>
            </div>
            <div className="ProducInfo">
                <Personicons adults={NumAdults} kids={NumChilds}/>
            </div>
        </div>
    )
}
