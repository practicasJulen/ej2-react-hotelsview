import React from 'react'
import HotelHostType from './HotelHostType'
import HotelProductCapacity from './HotelProductCapacity'
import HotelProductOptions from './HotelProductOptions'
import HotelProductPrice from './HotelProductPrice'
import '../../Css/miniHotel/product.css'

export default function Product({product={}, addToBasket=f=>f, isBasket = false}) {
    const {Name, BoardName, Price, Currency, NumAdults, NumChilds, NoReembolsable, Cancelation } = product
    const hostType = {Name, BoardName}
    const productCapacity = {NumAdults, NumChilds}
    const options = {NoReembolsable, Cancelation}
    const productPrice = {Currency, Price}

    return (
        <div className="Product">
            <HotelHostType {...hostType}/>
            <HotelProductCapacity {...productCapacity}/>
            { isBasket ? null : <HotelProductOptions {...options}/> }
            { isBasket ? null : <HotelProductPrice {...productPrice} product={product} addToBasket={addToBasket}/> }
        </div>
    )
}
