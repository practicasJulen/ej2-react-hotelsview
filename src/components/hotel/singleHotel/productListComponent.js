import React from 'react'
import Product from './product'

export default function ProductListComponent({productList={}, idHotel, addToBasket = f =>f}) {
    
    return (
        <>
        {
            productList.map( (o, i) => (
                <div key={i} className="a" style={{display:"flex", flex:1, flexGrow:1, flexDirection: "column", paddingBottom: "5%"}}> 
                    <Product product={{...o, idHotel}} addToBasket={addToBasket} key={i}/>
                </div>
            )
            )
        }
        </>
    )
}
