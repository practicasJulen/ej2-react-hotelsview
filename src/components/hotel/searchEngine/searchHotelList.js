import React from 'react'
import { FaHotel } from 'react-icons/fa'

export default function SearchHotelList({data:{hotels, onClick=f=>f}}) {
    return (
        <>
            <div className='ItemList'>
                <div style={{backgroundColor:'white', fontSize:14, color:'black'}}><p>HOTELES</p></div>
                {
                    hotels.map( (hotel, i) => (
                        <div  onClick={() => onClick(hotel.name)}  key={i}>
                            
                            <p className='Item'> {hotel.name}<FaHotel /></p>
                        </div>
                    ))
                }    
            </div>
        </>
    )
}
