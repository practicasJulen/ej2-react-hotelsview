
import React, {useReducer, useMemo, useEffect, useState} from 'react'
import { BsPerson} from 'react-icons/bs'
import useGuestReducer from '../../../utils.js/reducers/searchGuestReducer'
import RoomAdults from './guestComponents/roomAdults'
import RoomKids from './guestComponents/roomKids'
import '../../Css/searchBar/guests.css'
import RoomRooms from './guestComponents/roomRooms'
import { guestSum } from '../../../utils.js/functions'

export default function SearchGuestsComp({setGuests = f =>f}) {
    const [expanded, expandedRed] = useReducer( expanded => !expanded, false)
    const {roomState, dispatchFunctions} = useGuestReducer()
    const [guests, reduceGuests] = useReducer(() => guestSum(roomState), 3)

    useEffect(() => {
        setGuests(roomState)
        reduceGuests()
    }, [roomState])

    return (
        <>
            <div aria-expanded={expanded} className="SearchColContainer" style={{maxWidth:"50%"}}>
                <div onClick={expandedRed} className="Container-ItemCol">
                    <div className='ColTop'>
                        <p style={{display: 'flex', flex: 1, alignSelf:'center', fontSize:"70%"}}>Huéspedes</p>

                    </div>
                    <div className='ColBot'>
                        <p>{roomState.length} Hab. / {guests} personas</p>
                    </div>
                </div>
                <div className='IconCol'>
                    <p><BsPerson size={25}/></p>
                </div>
            </div>
                {
                    expanded ? 
                    (
                        <div className='RoomForm'>
                            <p onClick={expandedRed}>Close</p>
                            {

                                roomState.map((room, i) => (
                                    <div className='RoomContainer' key={i}> 
                                        <RoomRooms data={{...room, index: i, add: () => dispatchFunctions.addRoom(), del: () => dispatchFunctions.deleteRoom(i)}} />
                                        <RoomAdults data={{...room, roomIndex: i, add: () => dispatchFunctions.addAdult(i), del: () => dispatchFunctions.deleteAdult(i) }} />
                                        <RoomKids data={{...room, roomIndex: i, add: () => dispatchFunctions.addkid(i), addAge: (kid, age) => dispatchFunctions.addKidAge(i, kid, age), del: (kid) => dispatchFunctions.deleteKid(i, kid)}}/>
                                    </div> ))
                            }
                            
                        </div>
                    ): null
                }
        </>
    )
}
