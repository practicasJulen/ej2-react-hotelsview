import React, { useState, useEffect } from 'react'
import { GrGlobe } from 'react-icons/gr'
import Autocomplete from 'react-autocomplete'
import cityList from '../../../data/cityList.json'
import { FaCity, FaHotel } from 'react-icons/fa'
import SearchHotelList from './searchHotelList'
import '../../Css/searchBar/autocomplete.css'


export default function SearchZoneComp({setCity = f => f, setHotel = f => f, hotel, city}) {
  
    const [hotels, setHotels] = useState(undefined)
    const [val, setVal] = useState('')
    const [tempCity, setTempCity] = useState('')

    useEffect( () => {
       setCity(tempCity)
    }, [hotel])

    useEffect( () => {
        setVal(city? hotel? `${city}, ${hotel}`: `${city}`: `${val}`)
    }, [city])

    const renderCities = (state, val) => {
        return (
            state.city.toLowerCase().indexOf(val.toLowerCase()) !== -1
        );
    }

    return (
        <>
            <div className="SearchColContainer" style={{maxWidth:"60%"}}>
                <div className='Container-ItemCol'>
                    <div className='ColTop'>
                        <p>Seleccionar Destino</p>
                    </div>
                    <div className='ColBot'>

                        <Autocomplete
                            value={val}
                            inputProps={{placeholder:'City, Hotel (optional)'}}
                            items={cityList}
                            getItemValue={item => item.city}
                            shouldItemRender={renderCities}
                            renderMenu={
                                item => (
                                    <div  className="ListContainer">
                                        <div  className="ItemList">
                                            <div  style={{backgroundColor:'white', fontSize:14, color:'black'}}><p>CIUDADES</p></div>
                                            {item}
                                        </div>
                                        <div className="ItemList">
                                            {
                                                hotels ? <SearchHotelList data={{hotels, onClick: (value) => setHotel(value) }}/> : null
                                            }           
                                        </div>
                                    </div>
                                )
                            }
                            renderItem={
                                (item, isHighlighted) =>
                                    <div key={item.city} onMouseOver={ () => ( setHotels(item.Hotels), setTempCity(item.city) ) } className={`item ${isHighlighted ? 'selected-item' : ''}`}>
                                        <p className='Item'><FaCity />{item.city}</p>
                                    </div>
                            }
                            onSelect={ (e) => (setCity(e), setHotel(''))}
                            onChange={ ({target}) => setVal(target.value) }
                        />
                    </div>
                </div>
                <div className='IconCol'>
                    <GrGlobe size={25}/>

                </div>
            </div>
        </>
    )
}
