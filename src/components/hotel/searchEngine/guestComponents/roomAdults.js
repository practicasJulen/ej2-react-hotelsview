import React from 'react'
import { FaMinus } from 'react-icons/fa'
import { GrAddCircle } from 'react-icons/gr'

export default function RoomAdults({data:{adults, roomIndex, add = f => f, del = f => f}}) {
    console.log(roomIndex, ':', adults)

    return (
        <div className="Container">
            <div className="Cont-text">
                <p> Adultos:  {adults}</p>
            </div>
            <div className='Cont-buttons'>
                <button type='button' onClick={ () => del() }><FaMinus /></button>
                <button type='button' onClick={ () => add() }><GrAddCircle /></button>
            </div>
        </div>
    )
}
