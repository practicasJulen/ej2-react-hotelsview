import React from 'react'
import { GrAddCircle } from 'react-icons/gr'
import { FaMinus } from 'react-icons/fa'
import Select from 'react-select'

export default function RoomKids({data:{kids, roomIndex, add=f=>f, addAge=f=>f, del=f=>f}}) {
    const options = []
    for(let i = 0; i < 18; i ++ ){
        options.push({value: i, label: i})
    }
    return (
        <>
            <div className="Container" style={{ borderBottom:"1px gray solid"}}>
                <div className='Cont-text'>
                    <p style={{display: 'flex', flex: 3}}>Niños: {kids.length}</p>
                </div>
                <div className='Cont-buttons'>
                    <button type='button' onClick={ () => add() }><GrAddCircle /></button>
                </div>

            </div>
            <div className="KidsContainer" >
                <div id="texto">
                    <p>Edad de los enjendros</p>
                </div>
                <div id="edades">
                    {
                        kids.map((kid, i) => (
                            <div key={i}> 
                                    <Select  placeholder={kid.age} onChange={ (option) => addAge(i, option.value)} options={options} />
                                    <button type='button' onClick={ () => del(i) }><FaMinus /></button>
                            </div>
                        ))
                    }

                </div>

            </div>
        </>
    )
}
