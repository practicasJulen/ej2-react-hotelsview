import React from 'react'
import { FaMinus } from 'react-icons/fa'
import { GrAddCircle } from 'react-icons/gr'

export default function RoomRooms({data:{index, add=f=>f, del=f=>f}}) {
    return (
        <div className='Container' style={{borderBottom:"1px solid gray", flex: 0.1}}>
            <div className='Cont-text'>
                <p style={{fontWeight:550, }}>Habitación {index + 1}</p>
            </div>
            <div className='Cont-buttons'>
                {
                    index > 0 ? <button type='button' onClick={ () => del()}><FaMinus /></button> : null
                }
                <button type='button' onClick={() =>add()}><GrAddCircle /></button>

            </div>

        </div>
    )
}
