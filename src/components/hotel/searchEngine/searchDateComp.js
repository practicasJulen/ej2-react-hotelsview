import React, {StyleSheet}from 'react'
import { BsCalendar } from 'react-icons/bs'
import { useInput } from '../../../utils.js/customHooks'

export default function SearchDateComp({dateFromProps={}, dateToProps={}}) {
    return (
        <>
            <div className="SearchColContainer">
                <div className='Container-ItemCol'>
                    <div className='ColTop'>
                        <p>Seleccionar fechas</p>
                    </div>
                    <div className='ColBot'>
                        <div style={{display:'flex', flexDirection:'column'}}>
                            <label>Desde</label>
                            <input {...dateFromProps} style={{border:'none', width: '90%'}}/>
                        </div>
                        <div style={{display:'flex', flexDirection:'column'}}>
                            <label>Hasta</label>
                            <input {...dateToProps} style={{border:'none', width: '90%'}}/>
                        </div>
                    </div>
                </div>

                <div className='IconCol'>
                    <BsCalendar size={25}/>
                </div>

            </div>

            
        </>

    )
}



