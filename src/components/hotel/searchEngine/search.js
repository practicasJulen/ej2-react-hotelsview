import React, { FC, useEffect,  useMemo, useState } from 'react'
import { BsSearch } from 'react-icons/bs'
import { useInput } from '../../../utils.js/customHooks'
import SearchDateComp from './searchDateComp'
import SearchGuestsComp from './searchGuestsComp'
import SearchZoneComp from './searchZoneComp'
import '../../Css/searchBar/search.css'
import { searchDataBuilder } from '../../../utils.js/functions'

export default function Search ({data=[]}){
    const [dateFromProps, refreshDateFrom] = useInput('', {type:"date"})
    const [dateToProps, refreshDateTo] = useInput('', {type:"date"})
    const [guests, setGuests] = useState('')
    const [city, setCity] = useState('')
    const [hotel, setHotel] = useState('')

    useEffect(() => {
        console.log(hotel)
        console.log(city)
    }, [hotel, city])

    const searchProps = {setGuests}
    const searchZoneProps = {setCity, setHotel, hotel, city}
    const searchDateProps = {dateFromProps, dateToProps}

    const onSubmit = e => {
        e.preventDefault()
        const x = searchDataBuilder(
            {data:
                {
                    dateFrom:dateFromProps.value, 
                    dateTo:dateToProps.value,
                    guests:guests,
                    city: city, 
                    hotel:hotel 
                }
            }
        )
        console.log(x)
        alert('done')
    }
    

    return (
        <>
        <form onSubmit={onSubmit} style={{backgroundColor: "#faaf05"}}>
            <div className='Search'>
                <div className="Search-col" style={{justifyContent:'flex-end'}}>
                    <SearchGuestsComp {...searchProps} />
                </div>
                <div className="Search-col" style={{justifyContent:'center'}}>
                    <SearchZoneComp  {...searchZoneProps}/>
                </div>
                <div className="Search-col" style={{justifyContent:'flex-start'}}>
                    <SearchDateComp {...searchDateProps} /> 
                </div>
                <div className="Button-col">
                    <input type="submit" value="Buscar"/>
                </div>
            </div>

        </form>

        </>
    )
}
