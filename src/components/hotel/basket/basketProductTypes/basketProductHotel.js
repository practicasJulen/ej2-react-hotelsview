import React from 'react'
import HotelShortInfo from './hotelShortInfo'
import ReservaShortInfo from './reservaShortInfo'

export default function BasketProductHotel({hotel={}, product={}}) {
    return (
        <>
            <HotelShortInfo  data={hotel} />
            <ReservaShortInfo  data={product} />
        </>
    )
}
