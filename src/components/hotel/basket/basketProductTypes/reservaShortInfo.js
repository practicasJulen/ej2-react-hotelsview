import React from 'react'
import Product from '../../singleHotel/product'

export default function ReservaShortInfo({data={}}) {
    return (
        <div style={{display:'flex', flexDirection:'row', border: 'gray 0.5px solid'}}>
            <Product product={data} isBasket={true} />
        </div>
    )
}
