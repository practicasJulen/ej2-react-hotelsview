import React from 'react'
import Stars from '../../../reusable/starsComponent'

export default function HotelShortInfo({data={}}) {
    const {NombreHotel, Dir, City, CategoryName } = data

    return (
        <div style={{ display:"flex", flex:1, flexDirection:"row", alignItems:"baseline", justifyContent:"space-around", maxWidth:"70%", fontSize:"18px", fontWeight:"bold"}}>
            <p>{NombreHotel}</p>
            <div style={{display:'flex', flexWrap:"nowrap"}}>
                <Stars stars={CategoryName}/>
            </div>
            {/* <p>{City}</p> */}
        </div>
    )
}
