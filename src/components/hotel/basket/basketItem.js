import React from 'react'
import { AiOutlineDelete } from 'react-icons/ai'
import Product from '../singleHotel/product'
import BasketProduct from './basketProduct'

export default function BasketItem({item={}, deleteThis=f=>f}) {
    const deleteItem = () => deleteThis(item.idBasket)

    return (
        <div style={{display:'flex', flex: 1, flexDirection:'row', flexWrap:'nowrap', borderBottom: "gray 1px solid", backgroundColor:"#f2efe6"}}>
            <div  style={{display: 'flex', flex: 0.75, flexDirection:"column", borderRight:"1px solid gray"}}>
                <BasketProduct product={item} />
            </div>
            <div  style={{display: 'flex', flex: 0.1, justifyContent:"center", alignItems:"center", borderRight:"1px solid gray"}}><p>QTY: 1</p></div>
            <div  style={{display: 'flex', flex: 0.15, justifyContent:"center", alignItems:"center", borderRight:"1px solid gray"}}><p>P/U: {parseInt(item.Price) +   item.Currency}</p></div>
            <div  style={{display: 'flex', flex: 0.1, justifyContent:"center",}}>
                <button onClick={() => deleteItem()}><AiOutlineDelete /></button>
            </div>
        </div>
    )
}


