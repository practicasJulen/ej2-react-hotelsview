import React from 'react'

export default function BasketTotalPrice({price, currency}) {
    return (
        <>
            <div style={{display: 'flex', flexDirection:"row", justifyContent:"center", border:"1px gray solid", alignItems:"baseline", backgroundColor:"#f2efe6"}}>
                <p style={{padding: "1%"}}>Precio Total:   </p>
                <p style={{padding: "1%", fontWeight:"bold", fontSize:"18px"}}>{price} {currency}</p>
            </div>
        </>
    )
}
