import React from 'react'
import hotels from '../../../data/hotelsdata.json'
import { arrayFindValueByField} from '../../../utils.js/functions'
import BasketProductHotel from './basketProductTypes/basketProductHotel'

export default function BasketProduct({product={}}) {
    
    const hotel = arrayFindValueByField(hotels, 'idHotel', product.idHotel)

    return (
        <> 
            <BasketProductHotel product={product} hotel={hotel}/>
        </>
    )
}
