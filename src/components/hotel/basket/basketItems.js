import React from 'react'
import { priceSum } from '../../../utils.js/functions'
import BasketItem from './basketItem'
import BasketTotalPrice from './basketTotalPrice'

export default function BasketItems({items=[], deleteItem=f=>f}) {
    const precioTotal = priceSum(items)
    return (
        <>
            {
                items.map((item, i) =>   <BasketItem key={i} item={item} deleteThis={deleteItem} />)
            }
            <BasketTotalPrice price={precioTotal} currency={ items.length > 0 ? items[0].Currency: null}/>
        </>  
    )
}
