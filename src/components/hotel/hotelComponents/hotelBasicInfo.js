import React, {} from 'react'
import Stars from '../../reusable/starsComponent'
import {GrLocationPin} from 'react-icons/gr'
import {BsMailbox, BsTelephone} from 'react-icons/bs'
import { setStringMaxLenght } from '../../../utils.js/functions'
import { FaCity } from 'react-icons/fa'
import '../../Css/hotelsCss/hotelBasicInfo.css'


export default function HotelBasicInfo({data = {}, renderDescr=true}){
    const {CategoryName, Dir, Tfno, ZipCode, NombreHotel, City, ShortDesc,StateName} = data
    const newShortDesc = setStringMaxLenght(ShortDesc, 400)
    return(
        <>
            <div className="BasicContainer">
                <div className="BCinfo">
                        <div className="a">
                            <h2 style={{fontSize: 20}}>{NombreHotel}</h2>
                            <p><FaCity size={20}/> {City}, {StateName}</p>
                            <p><GrLocationPin color={"red"}/> {Dir} , {ZipCode}</p>
                        </div>
                        <div className="b">
                            <p><Stars stars={CategoryName}/></p>
                            <p><BsTelephone />  {Tfno}</p>
                        </div>
                        <div className="a">
                        </div>
                </div>
                {
                    renderDescr ? (
                    <div className="BCDescription" >
                        <p style={{fontSize:11, backgroundColor: "#ebf4fa", border:"0.5px dotted #569cc4"}}>"{newShortDesc}"</p>
                    </div> ) : null

                }
            </div>
        </>
    )
}