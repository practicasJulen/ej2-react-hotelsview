import React, { useState } from 'react'
import ContactComponent from '../../reusable/contactComponent'
import UbicationComponent from '../../ubicacionComponent'
import '../../Css/hotelsCss/hotelAdditionalInfo.css'


export default function HotelAdditionalInfo({data={}}){
    const [render, setRender] = useState('Contacto')
    const {Tfno, Email, Fax} = data
    
    return(
        <>
            <div className="AdditionalContainer">
                <div id="buttons">
                    <button  onClick={ () => setRender('Contacto')}>Contacto</button>
                    <button onClick={ () => setRender('Ubicacion')}>Ubicacion</button>

                </div>
                <div id="icons">
                    {
                        render === 'Contacto' ? <ContactComponent options={{Tfno, Email, Fax}}/>:
                        render === 'Ubicacion' ? <UbicationComponent  data={data}/> : <p>nada</p>
                    }
                </div>
            </div>
        </>
    )
}