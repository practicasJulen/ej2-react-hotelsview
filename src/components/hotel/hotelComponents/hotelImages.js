import React, { useReducer } from 'react'
import { reducerIndex } from '../../../utils.js/reducers'
import {BsChevronDoubleRight, BsChevronDoubleLeft} from 'react-icons/bs'
import '../../Css/hotelsCss/hotelImages.css'


export default function HotelImages({images=[], imgW="250px", imgH="180px", actualIndex=0}){
    function handleImgE(e){
        e.target.src = "https://ithemes.com/wp-content/uploads/2016/10/funny-404-pages.jpg"
    }
    const initState = {
        max: images.length,
        min: 0,
        actual: actualIndex
    }
    const [indexState, updateIndex] = useReducer(reducerIndex, initState )
    
    return(
        <>
            <div className="ImageContainer">
                <div style={{display: "flex", flexDirection:"row"}}> 
                    <button id="buttons" onClick={ () => updateIndex({type:'preview'})}><BsChevronDoubleLeft style={{backgroundColor:"white"}}/></button>
                    <div style={{display:"flex", flexDirection:"column"}}>
                        <img  style={{width:imgW,height:imgH ,borderRadius:"10px"}}src={images[indexState.actual]} onError={ (e) => handleImgE(e)} id="image"></img>
                        <p style={{display:"flex", position:"absolute", backgroundColor:" rgba(255, 255, 255, 0.7)", borderRadius: 10, color:"grey", alignSelf:"end", justifyItems:"flex-end", fontWeight: "normal"}  }>{indexState.actual +1} / {images.length }</p>
                    </div>
                    <button id="buttons" onClick={ () => updateIndex({type:'next'})}><BsChevronDoubleRight style={{backgroundColor:"white"}}/></button>
                </div>
            </div>
        </>
    )
}