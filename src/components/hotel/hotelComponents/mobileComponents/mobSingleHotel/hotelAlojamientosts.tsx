import React from 'react'
import { FC, useEffect } from 'react'
import { FaMinus } from 'react-icons/fa'
import { GrAdd, GrHide } from 'react-icons/gr'
import { sortHotelBoardNamesByPrice, spliceArrayByBoardName } from '../../../../../utils.js/filters'
import { useBasket} from '../../../../../utils.js/reducers'
import {Producto} from '../../../../HotelInterfaces'
import { SHProductots } from './sHProductots'
import { SHProductTypeList } from './SHProductTypeList'

interface Props{
    ListaPrecios: Array<Producto>
}

export const HotelAlojamientosts: FC<Props> = ({ListaPrecios}) => {
    const {basketState, dispatchFunctions} = useBasket()
    useEffect(() => {
        console.log(basketState)
    }, [basketState])

    const productArrays =  sortHotelBoardNamesByPrice(ListaPrecios)

    return (
        <div style={{display: 'flex', flexDirection: 'column', alignItems:'center'}}>
            {
                productArrays.map((x : Array<Producto>, i: number) => (
                    <SHProductTypeList  productList={x} key={i} addToBasket={(producto) => dispatchFunctions.addToBasket(producto)}/>
                ))
            }
        </div>
    )
}


