import React, {useRef} from 'react'
import { FC } from 'react'
import { useRefCustom } from '../../../../../utils.js/customHooks'
import { Contacto, Hotel } from '../../../../HotelInterfaces'
import UbicationComponent from '../../../../reusable/ubicationComponent'
import ImagesComponent from '../../../singleHotel/imagesComponent'
import { HotelAlojamientosts } from './hotelAlojamientosts'
import '../../../../Css/mobile/singleHotel.css'
import ContactComponent from '../../../../reusable/contactComponent'
import Stars from '../../../../reusable/starsComponent'
import { useEffect } from 'react'



const SingleHotelTs: FC<Hotel> = (data: Hotel) => {
    const {Tfno, Email, Fax} = data
    const contactProps : Contacto = {Tfno, Email, Fax}

    const [titleBtnProps, titleRef]:  any | React.MutableRefObject<any> = useRefCustom('imagen')
    const [imageBtnProps, imageRef]:  any | React.MutableRefObject<any> = useRefCustom('imagen')
    const [productosBtnProps, productosRef]:  any | React.MutableRefObject<any> = useRefCustom('producto')
    const [ubicacionBtnProps, ubicacionRef]:  any | React.MutableRefObject<any> = useRefCustom('ubicacion')
    const [infoBtnProps, infoRef]:  any | React.MutableRefObject<any> = useRefCustom('info')

    useEffect(()=> {
        titleRef.current.scrollIntoView()
    })

    return (
        <>
            <div className='fixedHead'>
                <button {...imageBtnProps} >Imagenes</button>
                <button {...infoBtnProps}>Info</button>
                <button {...productosBtnProps} >Alojamiento</button>
                <button {...ubicacionBtnProps}  >Ubicacion</button>
            </div>
            <span className='title' ref={titleRef}>
                <span id='name'>{data.NombreHotel}</span>
                <span id='stars'><Stars stars={data.CategoryName}/></span>
            </span>
            <span ref={imageRef}>
                <ImagesComponent {...{images:data.ListFotos, qty: 2, mobile:true}}/>
            </span>
            <span ref={productosRef}>
                <HotelAlojamientosts  {...data} />
            </span>
            <span ref={infoRef}>
                <div className=' GeneralContainer Contact'>
                    <div id='titulo'>
                        <span >Contacto</span>
                    </div>
                    <div id='contactos'>
                        <ContactComponent {...{options: contactProps}}/>
                    </div>
                </div>
                <div className= 'GeneralContainer Description'>
                    <div id='titulo'>
                        <span>Descripción</span>
                    </div>
                    <div id=''></div>
                    <span style={{}}>"{data.ShortDesc}"</span>
                </div>
                
            </span>
            <span ref={ubicacionRef}>
                <UbicationComponent {...{...data, style: leafleetStyle}}/>
            </span>
        </>
    )
}


const leafleetStyle = {
    width: '95vw',
    height: '40vh',
    position: 'relative',
    alignSelf: 'center',
    marginBottom: 10
}

export default SingleHotelTs
