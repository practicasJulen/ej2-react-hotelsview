import React from 'react'
import { FC } from 'react'
import { Producto } from '../../../../HotelInterfaces'
import Personicons from '../../../../reusable/personIconsComponent'
import { Precio } from '../../../../reusable/precio'


export const SHProductots: FC<Producto> = (data: Producto) => {

    return (
        <div style={{maxWidth:"90vw", minHeight: '15vh', borderTop: 'gray 1px solid', display: 'flex', flexDirection:'column', margin: 10}}>
            <div style={{display: 'flex', flex: 1, flexDirection: 'row', alignItems:'center'}}>
                <div style={{display: 'flex', flex: 1}}>
                    <span>{data.Name}</span>
                </div>
                <div  style={{ display: 'flex', flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                    <Personicons adults={data.NumAdults} kids={data.NumChilds} />
                </div>
                <div className='' style={{display: 'flex', flexDirection:'column'}}>
                    <small>precio final</small>
                    <div>
                        <Precio  {...{...data}} />
                    </div>
                </div>  
            </div>
            <div style={{display: 'flex', flex: 1, flexDirection: 'row', marginBottom: 5}}>
                <div style={{display: 'flex', flex: 0.7}}>

                </div>
                <div style={{display: 'flex', flex: 0.3}}>
                    <button onClick={data.addToBasket} style={{backgroundColor: '#11a2f7', color: 'white', borderRadius: 10, border: 'none'}}>RESERVAR</button>
                </div>
            </div> 
        </div>
    )
}
