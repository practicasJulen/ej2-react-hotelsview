import React from 'react'
import { FC, useReducer } from 'react'
import { BiHide} from 'react-icons/bi'
import {Producto} from '../../../../HotelInterfaces'
import { SHProductots } from './sHProductots'

interface Props {
    productList: Array<Producto>
    addToBasket: (producto: Producto) => void

}

export const SHProductTypeList: FC<Props> = ({productList, addToBasket}) => {
    const [oculto, setOculto] = useReducer( oculto => !oculto ,true)
    return (
        <div style={{backgroundColor: 'white', border: 'gray 1px solid', marginBottom: 20, minWidth: "90vw"}}> 
            <div style={{display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent:'space-between', marginLeft: 10, marginRight:10}}> 
                <p style={{display: 'flex', fontSize: 20, fontWeight: 650, color: '#3c3c3c', alignItems:'center'}}>{productList[0].BoardName}</p>
                    <div style={{display: 'flex', justifySelf: 'flex-end',}}>
                        <span onClick={setOculto}><BiHide color={!oculto ? 'green' : 'black'}  size={30}/> </span>
                    </div>
            </div>
            {
                oculto ? null: 
                productList.map((producto: Producto, j: number) => <SHProductots {...{...producto, addToBasket:() => addToBasket(producto) , key:j}} />)
            }
        </div> 
    )
}
