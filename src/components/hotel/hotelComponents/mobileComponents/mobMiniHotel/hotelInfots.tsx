import React, { FC } from 'react'
import { FaCity } from 'react-icons/fa'
import { GrLocationPin } from 'react-icons/gr'
import Stars from '../../../../reusable/starsComponent'

interface Props {
    Dir: string
    Tfno: string
    NombreHotel: string
    CategoryName: string
    City: string
    
}

export const HotelInfots: FC<Props> = (data:Props) => {
    return (
        <div className='InfoContainer'>
            <div className='FirstRow'>
                <span id='name'><h3>{data.NombreHotel}</h3></span>
                <div id='stars'> <Stars stars={data.CategoryName} /></div>
            </div>
            <div className='SecondRow'>
                <div id='city'>
                    <div id='cityItem'><FaCity /></div>
                    <span id='cityItem'>{data.City}, {data.Dir}</span>
                </div>
            </div>
        </div>
    )
}
