import React, { FC } from 'react'

interface Props {
    precio:string
    moneda: string
    setView: () => any
}

export const HotelMinPriceTs: FC<Props> = ({moneda, precio, setView}) => {
    return (
        <div className='PriceContainer'>
            <span className='item' id='text'>Desde</span>
            <span className='item' id='price'>{precio}</span>
            <span className='item' id='currency'>{moneda}</span> 
        </div>
    )
}

