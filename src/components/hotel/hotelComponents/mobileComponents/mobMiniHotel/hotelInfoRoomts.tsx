import React from 'react'
import { FC } from 'react'

interface Props {
    sortedPriceList: Array<Array<{BoardName: string, Name: string }>>
    index: number
    index2: number
}

const HotelInfoRoomts: FC<Props> = (data: Props) => {
    return (
        <div style={{ display:'flex', flex:1, flexDirection:'column', marginTop: 4}}>
            <span style={{ display: 'flex', fontSize: 12, fontWeight:550}}>{data.sortedPriceList[data.index][data.index2].BoardName}</span>
            <span style={{ display: 'flex', fontSize: 10}}>{data.sortedPriceList[data.index][data.index2].Name}</span>
        </div>
    )
}

export default HotelInfoRoomts
