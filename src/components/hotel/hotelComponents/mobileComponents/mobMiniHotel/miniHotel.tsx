import React, { FC } from 'react'
import { sortHotelBoardNamesByPrice } from '../../../../../utils.js/filters'
import Hotel from '../../../hotelItem'
import { HotelImagesTs } from './hotelImages'
import { HotelInfots } from './hotelInfots'
import { HotelMinPriceTs } from './hotelMinPrice'

import '../../../../Css/mobile/hotelList.css'
import HotelInfoRoomts from './hotelInfoRoomts'

interface Hotel{
    ListFotos: Array<string>
    idHotel: string
    ListaPrecios: Array<any>
    Dir: string
    Tfno: string
    ZipCode: string
    NombreHotel: string
    StateName: string
    CategoryName: string
    City: string
}



interface usedPriceProps{
    Price: string
    Currency: string
    BoardName: string,
    Name: string
}

interface Props {
    hotel: Hotel;
    setView: (data:{idHotel: String}) => void
}


export const MiniHotelTs: FC<Props> = ({hotel:{idHotel, ListFotos, ListaPrecios, ...others}, setView}) => {
    const sortedPriceList: Array<Array<usedPriceProps>> = sortHotelBoardNamesByPrice(ListaPrecios)
    console.log(ListaPrecios[0])
    return ( 
        <div className='HotelCardContainer' onClick={() => setView({idHotel})}>
            {/* <button  onClick={() => setView({idHotel})}> hazme click a ver si funciono</button> */}
            <div className='ContainerLeft'>
                <HotelImagesTs images={ListFotos} key={'img'}  imageProps={{height:'120', width:'120'}} />
            </div>
            <div className='ContainerRight'>
                <HotelInfots {...others} />
                <HotelInfoRoomts {...{sortedPriceList, index: 0, index2:0}}  />
                <HotelMinPriceTs moneda={sortedPriceList[0][0].Currency} precio={sortedPriceList[0][0].Price} setView={() => setView({idHotel})} />
            </div>

        </div>
    )
}



