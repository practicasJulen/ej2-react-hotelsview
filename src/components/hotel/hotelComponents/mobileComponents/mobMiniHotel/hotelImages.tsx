import React, {FC, useReducer, useState} from 'react'

interface imageProps{
    width: string
    height: string
}
interface Props {
    images: Array<string>
    imageProps?: imageProps
}

export const HotelImagesTs: FC<Props> = ({images, imageProps={width:'300', height:'200'}}) => {
    const [imageIndx, nextImage] = useReducer(imageIndx => imageIndx + 1, 0)
    return (
        <div id='image'>
            <img src={images[imageIndx]} onError={nextImage} width={imageProps.width} height={imageProps.height}></img>
        </div>
    )
}
