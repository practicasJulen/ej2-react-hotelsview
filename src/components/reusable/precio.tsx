import React from 'react'
import { FC } from 'react'

interface Props {
    Price: string,
    Currency: string,
}

export const Precio:  FC<Props> = ({Currency, Price}) => {
    const precio: number = parseInt(Price)
    const moneda: string = Currency.toUpperCase()
    return (
        <>
            <span style={{fontSize: 24, fontWeight: 650, color: '#11a2f7', marginRight: 4}}>{precio}</span> 
            <span style={{fontSize: 14, fontWeight: 500}}>{moneda}</span>
        </>
    )
}
