import React from 'react'

export default function RegularForm({formFields, formSubmit = f => f}) {
    return (
        <div>
            <form onSubmit={ (e) => formSubmit(e)}>
                
                <input type="submit" />
            </form>
        </div>
    )
}
