import React from 'react'
import { useState } from 'react/cjs/react.development'
import { useInput } from '../../../utils.js/customHooks'
import { validateEmail, validateTlf } from '../../../utils.js/regex'
import '../../Css/forms/form.css'

export default function BasketForm({}) {
    const [nameProps, resetName] = useInput('', {})
    const [lastNameProps, resetLastName] = useInput('', {})
    const [emailProps, resetEmail] = useInput('', {})
    const [tlfProps, resetTlf] = useInput('', {})
    const [observacionesProps, resetObservaciones] = useInput('', {required:false})
    const [errorMessage, setErrorMessage] = useState('')

    const submitBasketForm = (e) => {
        try{
            e.preventDefault()
            console.log(tlfProps.value)
            if(!validateEmail.test(emailProps.value)) throw new Error(`invalid email`)
            if(!validateTlf.test(tlfProps.value)) throw new Error(`invalid Tlf`)
            setErrorMessage()
            resetEmail()
            resetLastName()
            resetTlf()
            resetObservaciones()
            resetName()
            alert('yahoooo')
        }catch(e){ 
            console.log(e)
            setErrorMessage(e.message)
        }
    }

    return (

        <>
        <div className="form-container">
            {
                errorMessage ? <p style={{fontSize:'20px', color:'red', fontWeight: 'bold'}}>{errorMessage}</p> : null
            }
            <form onSubmit={submitBasketForm}>
                <div className="form-row">
                    <div className="form-item-col">
                        <label>Nombre*</label>
                        <input placeholder={'nombre'} {...nameProps}/>
                    </div>
                    <div className="form-item-col">
                        <label>Primer apellido*</label>
                        <input {...lastNameProps}/>
                    </div>
                </div>
                <div className="form-row"> 
                    <div className="form-item-col">
                        <label>Email*</label>
                        <input placeholder="dir@domain.es" {...emailProps}/>
                    </div>
                    <div className="form-item-col">
                        <label>Telefono*</label>
                        <input placeholder={'+34943754586'} {...tlfProps}/>
                    </div>
                </div>
                <div className="form-row textarea"> 
                    <div className="form-item-col">
                        <label>Observaciones</label>
                        <textarea {...observacionesProps}  /> 

                    </div>
                </div>
                <div className="form-row">
                    <div style={{display:'flex', flex:1,}}> 
                        <input type="checkbox" required/>
                        <label>Acepto las condiciones</label>
                    </div>
                </div>
                <div className="form-row">
                    <input  id="submit" type="submit"/>
                </div>
                
                {/* <label>¿Puedo robarte los datos?</label>
                <input type="checkbox" required/> */}
            </form>
        </div>
        </>  
    )
}
