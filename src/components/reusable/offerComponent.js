import React, {} from 'react'
import Personicons from './personIconsComponent'


// éste componente podría reescribirse para reutilizarlo 

export default function Offer({data={}}) {
    const {Price, Currency, BoardName, Name, NumAdults, NumChilds} = data
    return(
        <div style={{display:"flex", flex:1, flexDirection:"row", alignItems:"center", borderTop:"1px #b4b4b4 solid", padding: "5px" }}>
            <div style={{display: "flex", flex:1}}>
                <div style={{display:"flex", flexDirection:"column", fontSize:"10px", alignItems:"center"}}>
                    <p>{BoardName}</p>
                    <p>{Name}</p>
                </div>
            </div>
            <div style={{display: "flex", flex:1}}>
                <Personicons adults={NumAdults} kids={NumChilds} />
            </div>
            <div style={{display:"flex", flex:1, flexDirection:"row", justifyContent: 'space-evenly', alignItems:"baseline"}}>
                <p style={{fontSize:"16px", fontWeight:"bold", fontStyle:"oblique"}}>{Price}</p>
                <p style={{fontSize:"12px"}}>{Currency}</p>
            </div>
        </div>
    )
}