import React from 'react'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'

export default function UbicationComponent({Lat, Long, Dir, style={width: "95%", height:"80%",  alignSelf:"center", }}) {

    return (
        <div style={style}> 
            <MapContainer   center={[Lat, Long]} zoom={13} scrollWheelZoom={true}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={[Lat, Long]}>
                    <Popup>
                        {Dir}
                    </Popup>
                </Marker>
            </MapContainer>
        </div>
    )
}
