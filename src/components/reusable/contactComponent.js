import React, {} from 'react'
import ContactOption from './contactOption'

export default function ContactComponent({options={}}){
    
    return(
        <>
             {
                Object.entries(options).map((option, index) => (
                    <ContactOption name={option[0]} data={option[1]} key={index}/>
                ))
             }
        </>
    )
}