import React from "react";

export default function ImageComponent({url, height, width, props = {}, value='novalue'}){
    
    return(
        <img src={url} alt={value} {...props} width={width} height={height} />
    )
}