import React from 'react'

export default function PriceComponent({data={}}) {
    console.log(data)
    const {Price, Currency, BoardName} = data
    return (
        <div style={{display: 'flex', flex: 1, flexDirection:"row", alignItems:"baseline", borderBottom:"1px grey solid"}}>
            <p style={{fontSize: "10px", flex: 0.3, alignSelf:"center", fontWeight:"bold", paddingLeft:"3%"}}>{BoardName} </p>
            <p  style={{flex: 0.3}} id="letrapequena">desde</p>
            <p style={{flex: 0.3}}id="cantidad">{parseInt(Price)}</p>
            <p style={{}}id="moneda">{Currency}</p>
        </div>
    )
}
