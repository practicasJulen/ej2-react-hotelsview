import '../Css/reusables/columnInfo.css'

import React from 'react'

export default function ColumnInformationComponent({ alojBtnPropsRef = f => f, ubiBtnPropsRef = f  => f}) {
    return (
        <div id="info" >
            <div id="infoCol">
                <p id="infoText">¿ Tienes dudas ?  Contáctanos</p>
            </div>
            <div id="infoCol">
                <button  {...alojBtnPropsRef}>Ver Alojamientos</button>
            </div>
            <div id="infoCol">
                <button   {...ubiBtnPropsRef}>Ver Ubicación</button>
            </div>
        </div> 
    )
}
