import React, {} from 'react'
import {AiFillStar, AiOutlineStar} from 'react-icons/ai'



export default function Stars ({stars}){
    const starsnw = stars.split('')
    return (

        <>
            {
                starsnw.map((star, index) =>(
                    star === '*' ? <AiFillStar color={"#a91a2a"} key={index}/> 
                    : <AiOutlineStar  key={index}/>
                ))
                
            }
        </>
    )
}