import React, {} from 'react'
import {FaChild} from 'react-icons/fa'
import {ImMan} from 'react-icons/im'
import { ArrayFromNumber } from '../../utils.js/functions'

export default function Personicons ({adults, kids}){
    const [adultArr, kidArr] = ArrayFromNumber(adults, kids)
    
    return( 
        <>
            { 
                adultArr.map((_, i) => (
                    <ImMan key={i + "adult"} size={20}/>
                )) 
            }
            { 
                kidArr.fill().map((_, i) => (
                    <FaChild key={i + "kid"} size={15}/>
                )) 
            }
        </>
    )
}