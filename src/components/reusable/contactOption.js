import React, {} from 'react'
import { BsTelephone} from 'react-icons/bs'
import { AiOutlineMail } from 'react-icons/ai'
import { FaFax} from 'react-icons/fa'


export default function ContactOption ({name, data}) {
    return(
        <>
        <div style={{display:"flex", flex: 1, fontSize:"14px", fontWeight: "bold" }}>
            { 
                name === 'Tfno'   && data  ?   <p><BsTelephone className='contactIcon'/> {data}</p>    : 
                name === 'Email'  && data  ?   <p><AiOutlineMail  className='contactIcon'/>{data}</p>    :
                name === 'Fax'    && data  ?   <p><FaFax  className='contactIcon'/> {data}</p>    : null    
            }
        </div>
        </>
    )
}