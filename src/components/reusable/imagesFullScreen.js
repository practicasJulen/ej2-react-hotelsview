import React, {  useState } from 'react'
import { AiFillCloseCircle } from 'react-icons/ai'
import HotelImages from '../hotel/hotelComponents/hotelImages'

export default function ImagesFullScreen({images=[], actIndex=0, close = f => f}) {
    return (
        <>
            <div className='fullImages' style={{display: 'flex', flexDirection:'column', position:'fixed', zIndex:999, width:'100vw', height:'100vh', flex: 1, alignItems: 'center', justifyContent:'center', backgroundColor: 'rgba(0, 0, 0, 0.69)'}}>
                <div style={{display: 'flex', alignSelf:'center', justifySelf:'flex-end'}}><AiFillCloseCircle size={40} color={"white"} onClick={ () => close()}/></div>
                <HotelImages images={images} imgW="80vw" imgH="40vh" actualIndex={parseInt(actIndex ) + 1}/>
            </div>
        </>

    )
}


