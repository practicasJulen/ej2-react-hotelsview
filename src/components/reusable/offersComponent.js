import React, {} from 'react'
import {  sortArrayByPrice } from '../../utils.js/functions'
import Offer from './offerComponent'


export default function Offers({data=[], cuantity}){
    const newData = sortArrayByPrice(data, cuantity)
    return(
        <>
            {
                newData.map((i, index) => (
                    <Offer data={i} key={index}/>
                ))
            }
        </>
    )
}