import React from 'react'
import hotelsData from '../data/hotelsdata.json'
import BasketPage from './hotel/basketPage'
import SingleHotelTs from './hotel/hotelComponents/mobileComponents/mobSingleHotel/singleHotelTs'
import Hotel from  './hotel/hotelItem'
import MiniHotel from './hotel/miniHotelComponent'
import SingleHotel from './hotel/singleHotel'

export function HotelList({renderWhat, setView = f => f, basket=[], basketReducer = f => f, testState}){
    const addToBasket = item => basketReducer({type:'add', payload:item})
    const deleteFromBasket =  value => basketReducer({type: 'delete', payload:{key:'idBasket', value}})
    const index = renderWhat.idHotel ? hotelsData.map( o => o.idHotel).indexOf(renderWhat.idHotel) : null
    console.log(hotelsData)
    return(
        <>
            <div style={{display: "flex", flex:20, flexDirection: 'column', alignContent:"space-evenly", backgroundColor:"#77abd4"}}>
                {
                    renderWhat.idHotel ? testState?  <SingleHotel key={"sing"} data={hotelsData[index]} addToBasket={addToBasket}/>:<SingleHotelTs {...hotelsData[index]} /> :
                    renderWhat === 'basket' ? <BasketPage key={"basket"} basketData={basket} deleteItem={deleteFromBasket} />:
                    hotelsData.map((hotel, index) => (
                        <div key={index} style={{display:"flex", justifyContent:"center", paddingTop: "20px"}}>
                            {
                                renderWhat === 'big' ? <Hotel key={index} data={hotel}  setView={setView}/>:
                                renderWhat === 'litte' ? <MiniHotel  key={index} data={hotel} setView={setView} testState={testState}/>:
                                null
                            }
                        </div>    
                    ))
                }
            </div>
        </>
    )
}