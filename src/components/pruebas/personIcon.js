import React, {} from "react"

export default function IconComponent({array, Icon}){

    return(
        array.map((_, i) => (
            <Icon key={i}/>
        ))
    )

}