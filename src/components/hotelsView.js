import React, { useEffect, useReducer, useState, useMemo } from 'react'
import { BsBasket } from 'react-icons/bs'
import { basketReducer } from '../utils.js/reducers'
import Search from './hotel/searchEngine/search'
import { HotelList } from './hotelList'
import dataPruebas from '../data/pruebas.json'


export default function HotelView(){
    const [view, setView] = useState('big')
    const [basketState, reduceBasket] = useReducer(basketReducer, [])
    const _basketState = useMemo(() => basketState, [basketState])
    const [testState, redTest] = useReducer(test => !test, true)

    

    return(
        <>
            <button onClick={() => setView('big')}>big</button>
            <button onClick={() => setView('litte')}>mini</button>
            <button onClick={() => setView('basket')}>{<BsBasket />} {_basketState.length} </button>
            <button onClick={redTest}>Test</button>
            {/* <Search /> */}
            <HotelList key={'HotelList'} renderWhat={view} setView={setView} basket={_basketState} basketReducer={reduceBasket} testState={testState}/>
        </>
    )
}